const expect = require('chai').expect;
const request = require('request');
const app = require('../src/server');
const port = 3000;

describe("Color code converter api", () => {
    let server = undefined;
    before("Start server", (done) => {
        server = app.listen(port, () => {
            console.log(`Server listening on localhost:${port}`);
            done();
        });
    });
    
    describe("RGB to hex conversion", () => {
        const url = `http://localhost:${port}/rgb-to-hex?r=255&g=255&b=255`;

        it("returns status 200", (done) => {
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
        it("returns the color in hex", (done) => {
            request(url, (error, response, body) => {
                expect(body).to.equal("ffffff");
                done();
            })
        });
    });

    describe("hex to RGB conversion", () => {
        const url = `http://localhost:${port}/hex-to-rgb?hex=ffffff`;

        it("returns status 200", (done) => {
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
        it("returns the color in RGB", (done) => {
            request(url, (error, response, body) => {
                expect(body).to.equal("255,255,255");
                done();
            });
        });
    });

    after("Close server", (done) => {
        server.close();
        done();
    });
});