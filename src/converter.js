const pad = (hex) => {
    return(hex.length === 1 ? "0" + hex : hex);
};

module.exports = {
    rgbToHex: (r,g,b) => {
        const redHex = r.toString(16);
        const greenHex = g.toString(16);
        const blueHex = b.toString(16);

        return pad(redHex) + pad(greenHex) + pad(blueHex);
    },
    hexToRGB: (hex) => {
        const rgb = parseInt(hex, 16);
        var r = (rgb >> 16) & 255;
        var g = (rgb >> 8) & 255;
        var b = rgb & 255;

        return `${r},${g},${b}`;
    }
};