const express = require('express');
const app = express();
const converter = require('./converter');
const port = 3000;

// endpoint localhost:3000/
app.get('/', (req, res) => res.send("Hello"));

// endpoint localhost:3000/rgb-to-hex?r=255&g=0&b=0
app.get('/rgb-to-hex', (req, res) => {
    const red = parseInt(req.query.r, 10);
    const green = parseInt(req.query.g, 10);
    const blue = parseInt(req.query.b, 10);

    const hex = converter.rgbToHex(red,green,blue);
    res.send(hex);
});

app.get('/hex-to-rgb', (req, res) => {
    const hex = req.query.hex;

    const rgb = converter.hexToRGB(hex);
    res.send(rgb);
});

if(process.env.NODE_ENV === 'test')
{
    module.exports = app;
}
else
{
    app.listen(port, () => console.log(`Server listenning on localhost:${port}`));
}